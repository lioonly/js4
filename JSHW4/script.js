// 1) Функції у програмуванні потрібні для повторного використання коду та структування програми.
// 2) Передавання аргументів у функцію дозволяє їй отримувати та обробляти різні значення, зробивши код більш універсальним та гнучким.
// 3) Оператор return використовується для повернення значення з функції. Він завершує виконання функції та повертає результат, який можна використовувати далі в програмі.

function calculate(num1, num2, operator) {
    let result;
  
    switch (operator) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        result = num1 / num2;
        break;
      default:
        console.log('Неможливо виконати операцію');
        return;
    }
  
    console.log(result);
  }
  
  const number1 = parseFloat(prompt('Введіть перше число:'));
  const number2 = parseFloat(prompt('Введіть друге число:'));
  const operator = prompt('Введіть мат операцію:');
  
  calculate(number1, number2, operator);
  